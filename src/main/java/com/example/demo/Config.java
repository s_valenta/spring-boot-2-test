package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ConfigProps.class)
public class Config {
	@Autowired
	private ConfigProps configProps;


	@Bean
	public String bar() {
		System.out.println(configProps.getBar());
		return configProps.getBar();
	}
}
